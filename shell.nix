{ pkgs ? import <nixpkgs> {} }:
pkgs.mkShell {
  # buildInputs is for dependencies you'd need "at run time",
  # were you to to use nix-build not nix-shell and build whatever you were working on
  buildInputs = [
     pkgs.rustup
     pkgs.cargo


     pkgs.pkg-config
     pkgs.openssl


     # for dev help
     pkgs.cargo-watch
     pkgs.cargo-insta
  ];
  # Set Environment Variables
  RUST_BACKTRACE = 1;
}

