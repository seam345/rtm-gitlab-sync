use inquire::Confirm;
use rememberthemilk::{Perms, RTMConfig, API};
use std::fs::File;
use std::io::prelude::*;
use std::path::Path;

const CONFIG_PATH: &str = "./config.txt";

#[tokio::main]
async fn main() {
    let mut rtm_api;
    match Path::new(CONFIG_PATH).try_exists() {
        Ok(true) => {
            let mut file = File::open(CONFIG_PATH).unwrap();
            let mut contents = String::new();
            file.read_to_string(&mut contents).unwrap();
            let config: RTMConfig = serde_json::from_str(&contents).unwrap();
            rtm_api = API::from_config(config);

            let mut file = File::create(CONFIG_PATH).unwrap();
            file.write_all(
                &serde_json::to_string(&rtm_api.to_config())
                    .unwrap()
                    .into_bytes(),
            )
            .unwrap();
            let tasks = rtm_api.get_all_tasks().await.unwrap();
            for list in tasks.list {
                if let Some(v) = list.taskseries {
                    for ts in v {
                        println!("  {}", ts.name);
                    }
                }
            }
        }
        Err(_) | Ok(false) => {
            // Create the API object
            rtm_api = API::new(env!("API_KEY").to_string(), env!("API_SECRET").to_string());

            // Begin authentication using your API key
            let auth = rtm_api.start_auth(Perms::Read).await.unwrap();

            // write!("{}", auth.url);
            let ans = Confirm::new(&format!("Please visit {}?", auth.url))
                .with_default(false)
                .prompt();

            match ans {
                Ok(true) => println!("That's awesome!"),
                Ok(false) => println!("That's too bad, I've heard great things about it."),
                Err(_) => println!("Error with questionnaire, try again later"),
            }
            // auth.url is a URL which the user should visit to authorise the application
            // using their rememberthemilk.com account.  The user needs to visit this URL
            // and sign in before continuing below.
            if rtm_api.check_auth(&auth).await.unwrap() {
                // Successful authentication!  Can continue to use the API now.
                let mut file = File::create(CONFIG_PATH).unwrap();
                file.write_all(
                    &serde_json::to_string(&rtm_api.to_config())
                        .unwrap()
                        .into_bytes(),
                )
                .unwrap();
                let tasks = rtm_api.get_all_tasks().await.unwrap();
                for list in tasks.list {
                    if let Some(v) = list.taskseries {
                        for ts in v {
                            println!("  {}", ts.name);
                        }
                    }
                }
            } else {
                println!("failed to auth")
            }
        }
    }
}
