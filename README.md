# Remember the milk - Gitlab sync

Goal of the project is to be a long running deamon that snc RTM with Gitlab


## Git stanndard

I'm trying to stick to a [conventional commit standard](https://www.conventionalcommits.org) with my own definitions bellow
- `Spec(<scope>):` Testing something out that will maybe get incorporated later
  - Ideally would get squashed out but I know myself and sometimes it's not worth the effort of untangling
- `Docs:` Change to documentation